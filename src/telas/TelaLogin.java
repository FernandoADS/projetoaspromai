package telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.border.LineBorder;
import java.awt.Toolkit;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.DropMode;

public class TelaLogin extends JFrame {

	private JPanel contentPane;
	private JPasswordField inputSenha;
	private JLabel lblNewLabel;
	private JTextField inputNomeUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaLogin frame = new TelaLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaLogin() {
		setFont(new Font("Arial", Font.PLAIN, 12));
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Haysa Rodrigues\\Desktop\\asproma.jpg"));
		setTitle("Autentica\u00E7\u00E3o de Usu\u00E1rio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 400);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new LineBorder(Color.WHITE, 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		inputSenha = new JPasswordField();
		inputSenha.setBounds(75, 193, 234, 27);
		contentPane.add(inputSenha);
		
		JLabel lblUsurio = new JLabel("Nome do usu\u00E1rio");
		lblUsurio.setFont(new Font("Arial", lblUsurio.getFont().getStyle() | Font.BOLD, lblUsurio.getFont().getSize() + 2));
		lblUsurio.setForeground(new Color(0, 0, 0));
		lblUsurio.setBounds(75, 100, 132, 20);
		contentPane.add(lblUsurio);
		
		lblNewLabel = new JLabel("Senha do usu\u00E1rio");
		lblNewLabel.setFont(new Font("Arial", lblNewLabel.getFont().getStyle() | Font.BOLD, lblNewLabel.getFont().getSize() + 2));
		lblNewLabel.setBounds(75, 166, 132, 14);
		contentPane.add(lblNewLabel);
		
		inputNomeUsuario = new JTextField();
		inputNomeUsuario.setForeground(new Color(0, 0, 0));
		inputNomeUsuario.setDropMode(DropMode.INSERT);
		inputNomeUsuario.setFont(new Font("Arial", inputNomeUsuario.getFont().getStyle(), inputNomeUsuario.getFont().getSize() + 1));
		inputNomeUsuario.setBounds(75, 128, 234, 27);
		contentPane.add(inputNomeUsuario);
		inputNomeUsuario.setColumns(10);
		
		JButton clicarEntrar = new JButton("Entrar");
		clicarEntrar.setIcon(new ImageIcon(TelaLogin.class.getResource("/icons/icon_unlock.png")));
		clicarEntrar.setBounds(139, 245, 105, 35);
		contentPane.add(clicarEntrar);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon("C:\\Users\\Haysa Rodrigues\\Google Drive\\3\u00BA Per\u00EDodo\\Projeto Interdisciplinar I\\Projeto ASPROMA\\asproma (2).jpg"));
		label.setBounds(85, 33, 299, 312);
		contentPane.add(label);
	
		
	}
}
