package telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

public class TelaGerenciarUsuario extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnCadastrarUsuario;
	private JButton btnCancelar;
	private JButton btnExcluirUsuario;
	private JTextField inputTelefoneUsuario;
	private JTextField inputEmailUsuario;
	private JTextField inputCpfUsuario;
	private JTextField inputNomeUsuario;
	private JPasswordField inputSenhaUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaGerenciarUsuario frame = new TelaGerenciarUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaGerenciarUsuario() {
		setTitle("Gerenciador de Usu\u00E1rio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1050, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(102, 205, 170));
		panel.setBounds(0, 0, 1034, 211);
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnCadastrarUsuario = new JButton("Cadastrar");
		btnCadastrarUsuario.setIcon(new ImageIcon(TelaGerenciarUsuario.class.getResource("/icons/iconmais.png")));
		btnCadastrarUsuario.setFont(new Font("Arial", btnCadastrarUsuario.getFont().getStyle() | Font.BOLD, btnCadastrarUsuario.getFont().getSize()));
		btnCadastrarUsuario.setBounds(10, 177, 115, 23);
		panel.add(btnCadastrarUsuario);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(TelaGerenciarUsuario.class.getResource("/icons/arrow_left.png")));
		btnCancelar.setFont(new Font("Arial", btnCancelar.getFont().getStyle() | Font.BOLD, btnCancelar.getFont().getSize()));
		btnCancelar.setBounds(149, 177, 115, 23);
		panel.add(btnCancelar);
		
		btnExcluirUsuario = new JButton("Excluir");
		btnExcluirUsuario.setIcon(new ImageIcon(TelaGerenciarUsuario.class.getResource("/icons/iconDelete.png")));
		btnExcluirUsuario.setFont(new Font("Arial", btnExcluirUsuario.getFont().getStyle() | Font.BOLD, btnExcluirUsuario.getFont().getSize()));
		btnExcluirUsuario.setBounds(918, 177, 106, 23);
		panel.add(btnExcluirUsuario);
		
		inputTelefoneUsuario = new JTextField();
		inputTelefoneUsuario.setFont(new Font("Arial", inputTelefoneUsuario.getFont().getStyle(), inputTelefoneUsuario.getFont().getSize() + 1));
		inputTelefoneUsuario.setBounds(10, 132, 254, 29);
		panel.add(inputTelefoneUsuario);
		inputTelefoneUsuario.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setFont(new Font("Arial", lblTelefone.getFont().getStyle() | Font.BOLD, lblTelefone.getFont().getSize() + 1));
		lblTelefone.setBounds(10, 116, 63, 15);
		panel.add(lblTelefone);
		
		inputEmailUsuario = new JTextField();
		inputEmailUsuario.setFont(new Font("Arial", inputEmailUsuario.getFont().getStyle(), inputEmailUsuario.getFont().getSize() + 1));
		inputEmailUsuario.setBounds(316, 21, 254, 29);
		panel.add(inputEmailUsuario);
		inputEmailUsuario.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Arial", lblEmail.getFont().getStyle() | Font.BOLD, lblEmail.getFont().getSize() + 1));
		lblEmail.setBounds(316, 0, 77, 25);
		panel.add(lblEmail);
		
		inputCpfUsuario = new JTextField();
		inputCpfUsuario.setFont(new Font("Arial", inputCpfUsuario.getFont().getStyle(), inputCpfUsuario.getFont().getSize() + 1));
		inputCpfUsuario.setBounds(10, 81, 254, 29);
		panel.add(inputCpfUsuario);
		inputCpfUsuario.setColumns(10);
		
		inputNomeUsuario = new JTextField();
		inputNomeUsuario.setFont(new Font("Arial", inputNomeUsuario.getFont().getStyle(), inputNomeUsuario.getFont().getSize() + 1));
		inputNomeUsuario.setBounds(10, 21, 254, 29);
		panel.add(inputNomeUsuario);
		inputNomeUsuario.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Arial", lblNome.getFont().getStyle() | Font.BOLD, lblNome.getFont().getSize() + 1));
		lblNome.setBounds(10, 6, 46, 14);
		panel.add(lblNome);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setFont(new Font("Arial", lblCpf.getFont().getStyle() | Font.BOLD, lblCpf.getFont().getSize() + 1));
		lblCpf.setBounds(10, 61, 46, 14);
		panel.add(lblCpf);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Arial", lblSenha.getFont().getStyle() | Font.BOLD, lblSenha.getFont().getSize() + 1));
		lblSenha.setBounds(316, 61, 63, 14);
		panel.add(lblSenha);
		
		inputSenhaUsuario = new JPasswordField();
		inputSenhaUsuario.setFont(new Font("Arial", inputSenhaUsuario.getFont().getStyle(), inputSenhaUsuario.getFont().getSize() + 1));
		inputSenhaUsuario.setBounds(316, 81, 254, 29);
		panel.add(inputSenhaUsuario);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 211, 1034, 351);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setFont(new Font("Arial", table.getFont().getStyle() | Font.BOLD, table.getFont().getSize()));
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"Nome", "CPF", "E-mail", "Telefone"
			}
		));
		scrollPane.setViewportView(table);
	}
}
