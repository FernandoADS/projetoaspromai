package telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;

import java.awt.Component;

import javax.swing.JMenu;
import java.awt.Toolkit;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;

public class TelaPrincipal extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPrincipal() {
		setTitle("Gerenciamento de Conte�do");
		setIconImage(Toolkit
				.getDefaultToolkit()
				.getImage(
						"C:\\Users\\Haysa Rodrigues\\Google Drive\\3\u00BA Per\u00EDodo\\Projeto Interdisciplinar I\\Projeto ASPROMA\\asproma.PNG"));
		getContentPane().setBackground(Color.WHITE);
		
		JButton btnNewButton = new JButton("Sair");
		btnNewButton.setFont(new Font("Arial", btnNewButton.getFont().getStyle() | Font.BOLD, btnNewButton.getFont().getSize()));
		btnNewButton.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icons/icon_Exit.png")));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(927, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(392, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 487);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(new Font("Arial", menuBar.getFont().getStyle() | Font.BOLD, menuBar.getFont().getSize() + 1));
		setJMenuBar(menuBar);

		JMenu mnColeta = new JMenu("Coleta");
		mnColeta.setFont(new Font("Arial", mnColeta.getFont().getStyle()
				| Font.BOLD, mnColeta.getFont().getSize()));
		menuBar.add(mnColeta);

		JMenuItem mntmCadastrarNovaColeta = new JMenuItem(
				"Cadastrar e gerenciar coletas");
		mntmCadastrarNovaColeta.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icons/icon_clipboard.png")));
		mntmCadastrarNovaColeta.setFont(new Font("Arial", mntmCadastrarNovaColeta.getFont().getStyle() | Font.BOLD, mntmCadastrarNovaColeta.getFont().getSize()));
		mnColeta.add(mntmCadastrarNovaColeta);

		JMenu mnNcleo = new JMenu("Nucleo");
		mnNcleo.setFont(new Font("Arial", mnNcleo.getFont().getStyle()
				| Font.BOLD, mnNcleo.getFont().getSize()));
		menuBar.add(mnNcleo);

		JMenuItem mntmCadastrarNcleo = new JMenuItem("Cadastrar e gerenciar n\u00FAcleos");
		mntmCadastrarNcleo.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icons/icon_clipboard.png")));
		mntmCadastrarNcleo.setFont(new Font("Arial", mntmCadastrarNcleo.getFont().getStyle() | Font.BOLD, mntmCadastrarNcleo.getFont().getSize()));
		mnNcleo.add(mntmCadastrarNcleo);

		JMenu mnMaterial = new JMenu("Material");
		mnMaterial.setFont(new Font("Arial", mnMaterial.getFont().getStyle()
				| Font.BOLD, mnMaterial.getFont().getSize()));
		menuBar.add(mnMaterial);

		JMenuItem mntmCadastrarMaterial = new JMenuItem("Cadastrar e gerenciar material");
		mntmCadastrarMaterial.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icons/icon_clipboard.png")));
		mntmCadastrarMaterial.setFont(new Font("Arial", mntmCadastrarMaterial.getFont().getStyle() | Font.BOLD, mntmCadastrarMaterial.getFont().getSize()));
		mnMaterial.add(mntmCadastrarMaterial);

		JMenu mnFornecedor = new JMenu("Fornecedor");
		mnFornecedor.setFont(new Font("Arial", mnFornecedor.getFont()
				.getStyle() | Font.BOLD, mnFornecedor.getFont().getSize()));
		menuBar.add(mnFornecedor);

		JMenuItem mntmCadastrarFornecedor = new JMenuItem(
				"Cadastrar e gerenciar fornecedor");
		mntmCadastrarFornecedor.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icons/icon_clipboard.png")));
		mntmCadastrarFornecedor.setFont(new Font("Arial", mntmCadastrarFornecedor.getFont().getStyle() | Font.BOLD, mntmCadastrarFornecedor.getFont().getSize()));
		mnFornecedor.add(mntmCadastrarFornecedor);

		JMenu mnAssociado = new JMenu("Associado");
		mnAssociado.setFont(new Font("Arial", mnAssociado.getFont().getStyle()
				| Font.BOLD, mnAssociado.getFont().getSize()));
		menuBar.add(mnAssociado);

		JMenuItem mntmCadastrarAssociado = new JMenuItem("Cadastrar e gerenciar associados");
		mntmCadastrarAssociado.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icons/icon_clipboard.png")));
		mntmCadastrarAssociado.setFont(new Font("Arial", mntmCadastrarAssociado.getFont().getStyle() | Font.BOLD, mntmCadastrarAssociado.getFont().getSize()));
		mnAssociado.add(mntmCadastrarAssociado);

		JMenu mnDupla = new JMenu("Dupla");
		mnDupla.setFont(new Font("Arial", mnDupla.getFont().getStyle()
				| Font.BOLD, mnDupla.getFont().getSize()));
		menuBar.add(mnDupla);

		JMenuItem mntmCadastrarNovaDupla = new JMenuItem("Cadastrar e gerenciar duplas");
		mntmCadastrarNovaDupla.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icons/icon_clipboard.png")));
		mntmCadastrarNovaDupla.setFont(new Font("Arial", mntmCadastrarNovaDupla.getFont().getStyle() | Font.BOLD, mntmCadastrarNovaDupla.getFont().getSize()));
		mnDupla.add(mntmCadastrarNovaDupla);

		JMenu mnUsurio = new JMenu("Usuario");
		mnUsurio.setFont(new Font("Arial", mnUsurio.getFont().getStyle()
				| Font.BOLD, mnUsurio.getFont().getSize()));
		menuBar.add(mnUsurio);

		JMenuItem mntmCadastrarUsurio = new JMenuItem("Cadastrar e gerenciar usu\u00E1rios");
		mntmCadastrarUsurio.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/icons/peopleincon.png")));
		mntmCadastrarUsurio.setFont(new Font("Arial", mntmCadastrarUsurio.getFont().getStyle() | Font.BOLD, mntmCadastrarUsurio.getFont().getSize()));
		mnUsurio.add(mntmCadastrarUsurio);

	}
}
