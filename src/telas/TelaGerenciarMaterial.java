package telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;

public class TelaGerenciarMaterial extends JFrame {

	private JPanel contentPane;
	private JTextField campoNomeMaterial;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaGerenciarMaterial frame = new TelaGerenciarMaterial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaGerenciarMaterial() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Haysa Rodrigues\\Google Drive\\3\u00BA Per\u00EDodo\\Projeto Interdisciplinar I\\Projeto ASPROMA\\asproma.PNG"));
		setTitle("Cadastro de Material");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 475);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNome = new JLabel(" Nome do material");
		lblNome.setFont(lblNome.getFont().deriveFont(lblNome.getFont().getStyle() | Font.BOLD, lblNome.getFont().getSize() + 2f));
		lblNome.setBounds(10, 11, 209, 20);
		contentPane.add(lblNome);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(102, 205, 170));
		panel.setBounds(0, 0, 1008, 123);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(TelaGerenciarMaterial.class.getResource("/icons/arrow_left.png")));
		btnCancelar.setFont(new Font("Arial", btnCancelar.getFont().getStyle() | Font.BOLD, btnCancelar.getFont().getSize()));
		btnCancelar.setBounds(166, 82, 122, 23);
		panel.add(btnCancelar);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setIcon(new ImageIcon(TelaGerenciarMaterial.class.getResource("/icons/iconmais.png")));
		btnCadastrar.setFont(new Font("Arial", btnCadastrar.getFont().getStyle() | Font.BOLD, btnCadastrar.getFont().getSize()));
		btnCadastrar.setBounds(10, 82, 122, 23);
		panel.add(btnCadastrar);
		
		campoNomeMaterial = new JTextField();
		campoNomeMaterial.setBounds(10, 45, 298, 26);
		panel.add(campoNomeMaterial);
		campoNomeMaterial.setColumns(10);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.setIcon(new ImageIcon(TelaGerenciarMaterial.class.getResource("/icons/icon_delete.png")));
		btnExcluir.setFont(new Font("Arial", btnExcluir.getFont().getStyle() | Font.BOLD, btnExcluir.getFont().getSize()));
		btnExcluir.setBounds(889, 82, 109, 23);
		panel.add(btnExcluir);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 122, 1008, 315);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
				{null},
			},
			new String[] {
				"Materiais"
			}
		));
		scrollPane.setViewportView(table);
	}
}
