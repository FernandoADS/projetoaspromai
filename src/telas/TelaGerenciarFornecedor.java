package telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class TelaGerenciarFornecedor extends JFrame {

	private JPanel contentPane;
	private JTextField inputNomeFornecedor;
	private JTextField inputContatoFornecedor;
	private JTextField InputEnderecoFornecedor;
	private JTextField inputCNPJfornecedor;
	private JLabel lblContato;
	private JLabel lblEndereo;
	private JLabel lblCnpjDoFornecedor;
	private JTable tableFornecedor;
	private JButton btnExcluir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaGerenciarFornecedor frame = new TelaGerenciarFornecedor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaGerenciarFornecedor() {
		setTitle("Gerenciador de Fornecedores");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Haysa Rodrigues\\Desktop\\asproma.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(102, 205, 170));
		panel.setBounds(0, 0, 1008, 284);
		contentPane.add(panel);
		panel.setLayout(null);
		
		inputNomeFornecedor = new JTextField();
		inputNomeFornecedor.setBounds(10, 36, 421, 29);
		panel.add(inputNomeFornecedor);
		inputNomeFornecedor.setColumns(10);
		
		inputContatoFornecedor = new JTextField();
		inputContatoFornecedor.setColumns(10);
		inputContatoFornecedor.setBounds(10, 90, 421, 29);
		panel.add(inputContatoFornecedor);
		
		InputEnderecoFornecedor = new JTextField();
		InputEnderecoFornecedor.setColumns(10);
		InputEnderecoFornecedor.setBounds(10, 151, 421, 29);
		panel.add(InputEnderecoFornecedor);
		
		inputCNPJfornecedor = new JTextField();
		inputCNPJfornecedor.setColumns(10);
		inputCNPJfornecedor.setBounds(10, 210, 421, 29);
		panel.add(inputCNPJfornecedor);
		
		JButton btnCadastrarFornecedor = new JButton("Cadastrar");
		btnCadastrarFornecedor.setIcon(new ImageIcon(TelaGerenciarFornecedor.class.getResource("/icons/iconmais.png")));
		btnCadastrarFornecedor.setFont(new Font("Arial", btnCadastrarFornecedor.getFont().getStyle() | Font.BOLD, btnCadastrarFornecedor.getFont().getSize() + 1));
		btnCadastrarFornecedor.setBounds(10, 250, 122, 23);
		panel.add(btnCadastrarFornecedor);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(TelaGerenciarFornecedor.class.getResource("/icons/arrow_left.png")));
		btnCancelar.setFont(new Font("Arial", btnCancelar.getFont().getStyle() | Font.BOLD, btnCancelar.getFont().getSize() + 1));
		btnCancelar.setBounds(153, 250, 122, 23);
		panel.add(btnCancelar);
		
		JLabel lblNewLabel = new JLabel("Nome do Fornecedor");
		lblNewLabel.setFont(new Font("Arial", lblNewLabel.getFont().getStyle() | Font.BOLD, lblNewLabel.getFont().getSize() + 1));
		lblNewLabel.setBounds(10, 11, 177, 14);
		panel.add(lblNewLabel);
		
		lblContato = new JLabel("Contato");
		lblContato.setFont(new Font("Arial", lblContato.getFont().getStyle() | Font.BOLD, lblContato.getFont().getSize() + 1));
		lblContato.setBounds(10, 65, 77, 29);
		panel.add(lblContato);
		
		lblEndereo = new JLabel("Endere\u00E7o ");
		lblEndereo.setFont(new Font("Arial", lblEndereo.getFont().getStyle() | Font.BOLD, lblEndereo.getFont().getSize() + 1));
		lblEndereo.setBounds(10, 126, 88, 14);
		panel.add(lblEndereo);
		
		lblCnpjDoFornecedor = new JLabel("CNPJ do Fornecedor");
		lblCnpjDoFornecedor.setFont(new Font("Arial", lblCnpjDoFornecedor.getFont().getStyle() | Font.BOLD, lblCnpjDoFornecedor.getFont().getSize() + 1));
		lblCnpjDoFornecedor.setBounds(10, 185, 177, 14);
		panel.add(lblCnpjDoFornecedor);
		
		btnExcluir = new JButton("Excluir");
		btnExcluir.setIcon(new ImageIcon(TelaGerenciarFornecedor.class.getResource("/icons/iconDelete.png")));
		btnExcluir.setFont(new Font("Arial", btnExcluir.getFont().getStyle() | Font.BOLD, btnExcluir.getFont().getSize()));
		btnExcluir.setBounds(892, 251, 106, 23);
		panel.add(btnExcluir);
		
		JScrollPane scrollPaneFornecedor = new JScrollPane();
		scrollPaneFornecedor.setBounds(0, 286, 1008, 276);
		contentPane.add(scrollPaneFornecedor);
		
		tableFornecedor = new JTable();
		tableFornecedor.setFont(new Font("Arial", tableFornecedor.getFont().getStyle() | Font.BOLD, tableFornecedor.getFont().getSize()));
		tableFornecedor.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"Nome do Fornecedor", "Contato", "Endere\u00E7o", "CNPJ"
			}
		));
		scrollPaneFornecedor.setViewportView(tableFornecedor);
	}
}
