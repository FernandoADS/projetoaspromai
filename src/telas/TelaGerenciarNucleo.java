package telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Toolkit;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;

public class TelaGerenciarNucleo extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField inputNomeNucleo;
	private JTextField inputTelNucleo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaGerenciarNucleo frame = new TelaGerenciarNucleo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaGerenciarNucleo() {
		setTitle("Gerenciamento de N\u00FAcleo");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Haysa Rodrigues\\Desktop\\asproma.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 475);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1008, 168);
		panel.setBackground(new Color(102, 205, 170));
		contentPane.add(panel);
		panel.setLayout(null);
		
		inputNomeNucleo = new JTextField();
		inputNomeNucleo.setBounds(10, 27, 254, 26);
		panel.add(inputNomeNucleo);
		inputNomeNucleo.setColumns(10);
		
		inputTelNucleo = new JTextField();
		inputTelNucleo.setColumns(10);
		inputTelNucleo.setBounds(10, 87, 254, 26);
		panel.add(inputTelNucleo);
		
		JLabel lblNomeDoNcleo = new JLabel("Nome do n\u00FAcleo");
		lblNomeDoNcleo.setFont(new Font("Arial", lblNomeDoNcleo.getFont().getStyle() | Font.BOLD, lblNomeDoNcleo.getFont().getSize() + 1));
		lblNomeDoNcleo.setBounds(10, 11, 129, 14);
		panel.add(lblNomeDoNcleo);
		
		JLabel lblTelefoneDoNcleo = new JLabel("Telefone do n\u00FAcleo");
		lblTelefoneDoNcleo.setFont(new Font("Arial", lblTelefoneDoNcleo.getFont().getStyle() | Font.BOLD, lblTelefoneDoNcleo.getFont().getSize() + 1));
		lblTelefoneDoNcleo.setBounds(10, 64, 129, 14);
		panel.add(lblTelefoneDoNcleo);
		
		JButton btnNewButton = new JButton("Cadastrar");
		btnNewButton.setIcon(new ImageIcon(TelaGerenciarNucleo.class.getResource("/icons/iconmais.png")));
		btnNewButton.setFont(new Font("Arial", btnNewButton.getFont().getStyle() | Font.BOLD, btnNewButton.getFont().getSize()));
		btnNewButton.setBounds(10, 134, 119, 23);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cancelar");
		btnNewButton_1.setIcon(new ImageIcon(TelaGerenciarNucleo.class.getResource("/icons/arrow_left.png")));
		btnNewButton_1.setFont(new Font("Arial", btnNewButton_1.getFont().getStyle() | Font.BOLD, btnNewButton_1.getFont().getSize()));
		btnNewButton_1.setBounds(145, 134, 119, 23);
		panel.add(btnNewButton_1);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.setIcon(new ImageIcon(TelaGerenciarNucleo.class.getResource("/icons/iconDelete.png")));
		btnExcluir.setFont(new Font("Arial", btnExcluir.getFont().getStyle() | Font.BOLD, btnExcluir.getFont().getSize()));
		btnExcluir.setBounds(898, 134, 100, 23);
		panel.add(btnExcluir);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setToolTipText("Nome do N\u00FAcleo");
		scrollPane.setBounds(0, 167, 1008, 270);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
			},
			new String[] {
				"Nome do N�cleo", "Telefone"
			}
		));
		scrollPane.setViewportView(table);
	}
}
